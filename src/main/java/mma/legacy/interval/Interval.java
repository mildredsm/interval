package mma.legacy.interval;

import org.apache.log4j.Logger;

/**
 * Clase para el ejemplo de trabajo con Legacy
 * 
 * @author Agustin
 * 
 *         Controla operaciones sobre intervalos que pueden ser abiertos o
 *         cerrados, en uno o ambos extremos
 * 
 */
public class Interval {

	static Logger logger = Logger.getLogger(Interval.class);

	private double minimum; // Limite mínimo del intervalo
	private double maximum; // Limite máximo del intervalo
	private IntervalType intervalType; // Tipo de intervalo (LEFT_OPENED|RIGHT_OPENED|BOTH_OPENED|UNOPENED)

	/**
	 * Constructor de la clase
	 * 
	 * @param minimum
	 *            número que indica el limite superior del intervalo
	 * @param maximum
	 *            número que indica el limite superior del intervalo
	 * @param intervalType
	 *            tipo del intervalo
	 *            (LEFT_OPENED|RIGHT_OPENED|BOTH_OPENED|UNOPENED). No puede ser
	 *            nulo.
	 */
	public Interval(double minimum, double maximum, IntervalType intervalType) {
		if (intervalType == null)
			throw new IllegalArgumentException("IntervalType cannot be null");

		this.minimum = minimum;
		this.maximum = maximum;
		this.intervalType = intervalType;
		logger.info("Objeto creado");
	}

	/**
	 * Este método retorna el punto medio del intervalo
	 * 
	 * @return Retorna el punto medio del intervalo como un double
	 */
	public double calculateMidPoint() {
		return (maximum + minimum) / 2;
	}

	/**
	 * Este método evalúa si el parámetro value está dentro del intervalo
	 * 
	 * @param value
	 *            valor double, a evaluar si está dentro del intervalo
	 * 
	 * @return Retorna true si value está dentro del intervalo, false en caso
	 *         contrario.
	 */
	public boolean includesValue(double value) {
		logger.info("Objeto creado");

		switch (intervalType) {
		case BOTH_OPENED:
			return isValueInsideBothOpened(value);
		case LEFT_OPENED:
			return isValueInsideLeftOpened(value);
		case RIGHT_OPENED:
			return isValueInsideRightOpened(value);
		case UNOPENED:
			return isValueInsideUnopened(value);
		default:
			return false;
		}
	}

	/**
	 * Este método evalúa si el parámetro value está dentro del intervalo cuando
	 * este es BOTH_OPENED
	 * 
	 * @param value
	 *            valor double, a evaluar si está dentro del intervalo
	 * @return Retorna true si value está dentro del intervalo, false en caso
	 *         contrario.
	 */

	private boolean isValueInsideBothOpened(double value) {
		return minimum < value && value < maximum;
	}

	/**
	 * Este método evalúa si el parámetro value está dentro del intervalo cuando
	 * este es LEFT_OPENED
	 * 
	 * @param value
	 *            valor double, a evaluar si está dentro del intervalo
	 * @return Retorna true si value está dentro del intervalo, false en caso
	 *         contrario.
	 */

	private boolean isValueInsideLeftOpened(double value) {
		return minimum < value && value <= maximum;
	}

	/**
	 * Este método evalúa si el parámetro value está dentro del intervalo cuando
	 * este es RIGHT_OPENED
	 * 
	 * @param value
	 *            valor double, a evaluar si está dentro del intervalo
	 * @return Retorna true si value está dentro del intervalo, false en caso
	 *         contrario.
	 */

	private boolean isValueInsideRightOpened(double value) {
		return minimum <= value && value < maximum;
	}

	/**
	 * Este método evalúa si el parámetro value está dentro del intervalo cuando
	 * este es UNOPENED
	 * 
	 * @param value
	 *            valor double, a evaluar si está dentro del intervalo
	 * @return Retorna true si value está dentro del intervalo, false en caso
	 *         contrario.
	 */

	private boolean isValueInsideUnopened(double value) {
		return minimum <= value && value <= maximum;
	}

	/**
	 * Este método evalúa si el intervalo incluye a otro pasado como parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente contenido
	 * @return Retorna true si interval pasado como parámetro está dentro del
	 *         intervalo, false en caso contrario.
	 */
	public boolean includesInterval(Interval interval) {

		switch (this.intervalType) {
		case BOTH_OPENED:
			return includesIntervalWhenBothOpenedInterval(interval);
		case LEFT_OPENED:
			return includesIntervalWhenLeftOpenedInterval(interval);
		case RIGHT_OPENED:
			return includesIntervalWhenRightOpenedInterval(interval);
		case UNOPENED:
			return includesIntervalWhenUnopenedInterval(interval);
		default:
			return false;
		}

	}

	/**
	 * Este método evalúa si el intervalo (BOTH_OPENED) incluye a otro pasado como
	 * parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente contenido
	 * @return Retorna true si interval pasado como parámetro está dentro del
	 *         intervalo, false en caso contrario.
	 */
	private boolean includesIntervalWhenBothOpenedInterval(Interval interval) {
		switch (interval.intervalType) {
		case BOTH_OPENED:
			return isMinIncludedOrIsLimitAndMaxIncludedOrIsLimit(interval);
		case LEFT_OPENED:
			return isMinIncludedOrIsLimitAndMaxIncluded(interval);
		case RIGHT_OPENED:
			return isMinIncludedAndMinOrMaxAreLimits(interval);
		case UNOPENED:
			return isMinAndMaxIncluded(interval);
		default:
			return false;
		}
	}

	/**
	 * Este método evalúa si el intervalo (LEFT_OPENED) incluye a otro pasado como
	 * parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente contenido
	 * @return Retorna true si interval pasado como parámetro está dentro del
	 *         intervalo, false en caso contrario.
	 */
	private boolean includesIntervalWhenLeftOpenedInterval(Interval interval) {
		switch (interval.intervalType) {
		case BOTH_OPENED:
		case LEFT_OPENED:
			return isMinIncludedOrIsLimitAndMaxIncludedOrIsLimit(interval);
		case RIGHT_OPENED:
		case UNOPENED:
			return isMaxIncludedOrIsLimitAndMinIncluded(interval);
		default:
			return false;
		}
	}

	/**
	 * Este método evalúa si el intervalo (RIGHT_OPENED) incluye a otro pasado como
	 * parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente contenido
	 * @return Retorna true si interval pasado como parámetro está dentro del
	 *         intervalo, false en caso contrario.
	 */
	private boolean includesIntervalWhenRightOpenedInterval(Interval interval) {
		switch (interval.intervalType) {
		case BOTH_OPENED:
		case RIGHT_OPENED:
			return isMinIncludedOrIsLimitAndMaxIncludedOrIsLimit(interval);
		case LEFT_OPENED:
		case UNOPENED:
			return isMinIncludedOrIsLimitAndMaxIncluded(interval);
		default:
			return false;
		}
	}

	/**
	 * Este método evalúa si el intervalo (UNOPENED) incluye a otro pasado como
	 * parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente contenido
	 * @return Retorna true si interval pasado como parámetro está dentro del
	 *         intervalo, false en caso contrario.
	 */
	private boolean includesIntervalWhenUnopenedInterval(Interval interval) {
		switch (interval.intervalType) {
		case BOTH_OPENED:
		case LEFT_OPENED:
		case RIGHT_OPENED:
		case UNOPENED:
			return isMinIncludedOrIsLimitAndMaxIncludedOrIsLimit(interval);
		default:
			return false;
		}
	}

	private boolean isMinIncludedOrIsLimitAndMaxIncluded(Interval interval) {
		return isMinIncludedOrMinIsLimit(interval) && (this.includesValue(interval.maximum));
	}

	private boolean isMaxIncludedOrIsLimitAndMinIncluded(Interval interval) {
		return this.includesValue(interval.minimum) && isMaxIncludedOrMaxIsLimit(interval);
	}

	private boolean isMinAndMaxIncluded(Interval interval) {
		return (this.includesValue(interval.minimum)) && (this.includesValue(interval.maximum));
	}

	private boolean isMinIncludedAndMinOrMaxAreLimits(Interval interval) {
		return (this.includesValue(interval.minimum)) && isMaxIncludedOrMaxIsLimit(interval);
	}

	private boolean isMinIncludedOrIsLimitAndMaxIncludedOrIsLimit(Interval interval) {

		return isMinIncludedOrMinIsLimit(interval) && isMaxIncludedOrMaxIsLimit(interval);

	}

	private boolean isMaxIncludedOrMaxIsLimit(Interval interval) {
		return this.includesValue(interval.maximum) || Double.compare(maximum, interval.maximum) == 0;
	}

	private boolean isMinIncludedOrMinIsLimit(Interval interval) {
		return this.includesValue(interval.minimum) || Double.compare(minimum, interval.minimum) == 0;
	}

	/**
	 * Este método evalúa si el intervalo se intersecta con otro pasado como
	 * parámetro
	 * 
	 * @param interval
	 *            Intervalo potencialmente intersectado
	 * @return Retorna true si interval pasado como parámetro intersecta el
	 *         intervalo, false en caso contrario.
	 */

	public boolean intersectsWith(Interval interval) {
		if (Double.compare(minimum, interval.maximum) == 0) {
			return intersectsWhenTouchAtLeftLimit(interval);
		}
		if (Double.compare(maximum, interval.minimum) == 0) {
			return intersectsWhenTouchAtRightLimit(interval);
		}
		return this.includesValue(interval.minimum) || this.includesValue(interval.maximum);
	}

	/**
	 * Este método evalua si dos intervalos se tocan, en el caso en que el mínimo
	 * del primero coincida con el máximo del segundo
	 * 
	 * @param interval
	 *            Intervalo potencialmente intersectado
	 * @return Retorna true si interval pasado como parámetro intersecta el
	 *         intervalo, false en caso contrario.
	 */
	private boolean intersectsWhenTouchAtRightLimit(Interval interval) {
		switch (intervalType) {
		case BOTH_OPENED:
		case RIGHT_OPENED:
			return false;
		case LEFT_OPENED:
		case UNOPENED:
			return interval.intervalType == IntervalType.RIGHT_OPENED || interval.intervalType == IntervalType.UNOPENED;
		default:
			return false;
		}
	}

	/**
	 * Este método evalua si dos intervalos se tocan, en el caso en que el máximo
	 * del primero coincida con el mínimo del segundo
	 * 
	 * @param interval
	 *            Intervalo potencialmente intersectado
	 * @return Retorna true si interval pasado como parámetro intersecta el
	 *         intervalo, false en caso contrario.
	 */

	private boolean intersectsWhenTouchAtLeftLimit(Interval interval) {
		switch (intervalType) {
		case BOTH_OPENED:
		case LEFT_OPENED:
			return false;
		case RIGHT_OPENED:
		case UNOPENED:
			return interval.intervalType == IntervalType.LEFT_OPENED || interval.intervalType == IntervalType.UNOPENED;
		default:
			return false;
		}
	}

}
