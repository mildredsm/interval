package mma.legacy.interval;

/**
 * Esta clase es una factoria para crear intervalos de tipo
 * LEFT_OPENED|RIGHT_OPENED|BOTH_OPENED|UNOPENED
 * 
 * @author Agustin
 *
 */
public class IntervalFactory {

	/**
	 * Constructor (usado para enmascarar el público que se crea por defecto)
	 */
	private IntervalFactory() {

	}

	/**
	 * Este método crea un nuevo intervalo
	 * 
	 * @param minimum
	 *            número que indica el limite superior del intervalo
	 * @param maximum
	 *            número que indica el limite superior del intervalo
	 * @param intervalType
	 *            tipo del intervalo
	 *            (LEFT_OPENED|RIGHT_OPENED|BOTH_OPENED|UNOPENED). No puede ser
	 *            nulo.
	 */
	public static Interval getInterval(double minimum, double maximum, IntervalType intervalType) {
		return new Interval(minimum, maximum, intervalType);
	}
}
