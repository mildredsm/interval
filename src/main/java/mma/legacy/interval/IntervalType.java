package mma.legacy.interval;

/**
 * Esta clase define los posibles valores para un tipo de intervalo (LEFT_OPENED|RIGHT_OPENED|BOTH_OPENED|UNOPENED) d
 * @author Agustin
 *
 */

public enum IntervalType {
	
	LEFT_OPENED, 
	RIGHT_OPENED, 
	BOTH_OPENED, 
	UNOPENED;

}
