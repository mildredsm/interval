package mma.legacy.interval;

import static org.junit.Assert.*;

import org.junit.Before;

import mma.legacy.interval.Interval;
import mma.legacy.interval.IntervalFactory;
import mma.legacy.interval.IntervalType;

import org.junit.Test;

/**
 * Esta clase de test contiene una suite de test para comprobar el
 * funcionamiento correcto de los métodos de la clase Interval.java: 
 * Constructor, calculateMidPoint(), includesValue(), intersectsWith()
 * 
 * @author Agustin
 *
 */

public class IntervalTest {

	private Interval bothOpenedPivot;
	private Interval leftOpenedPivot;
	private Interval rightOpenedPivot;
	private Interval unopenedPivot;
	private Interval bothOpenedPivotForIntersection;
	private Interval leftOpenedPivotForIntersection;
	private Interval rightOpenedPivotForIntersection;
	private Interval unopenedPivotForIntersection;

	@Before
	public void setUp() throws Exception {
		bothOpenedPivot = IntervalFactory.getInterval(20, 35, IntervalType.BOTH_OPENED);
		leftOpenedPivot = IntervalFactory.getInterval(20, 35, IntervalType.LEFT_OPENED);
		rightOpenedPivot = IntervalFactory.getInterval(20, 35, IntervalType.RIGHT_OPENED);
		unopenedPivot = IntervalFactory.getInterval(20, 35, IntervalType.UNOPENED);
		bothOpenedPivotForIntersection = IntervalFactory.getInterval(20, 40, IntervalType.BOTH_OPENED);
		leftOpenedPivotForIntersection = IntervalFactory.getInterval(20, 40, IntervalType.LEFT_OPENED);
		rightOpenedPivotForIntersection = IntervalFactory.getInterval(20, 40, IntervalType.RIGHT_OPENED);
		unopenedPivotForIntersection = IntervalFactory.getInterval(20, 40, IntervalType.UNOPENED);
	}

	/*
	 * El siguiente test prueba que se lanza una excepción cuando el tipo del
	 * intervalo es nulo.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void throwExceptionWhenIntervalTypeIsNullTest() {
		IntervalFactory.getInterval(0, 10, null);
	}

	/*
	 * El siguiente test prueba el "cálculo del punto medio" para los 4 tipos de
	 * intervalos cuando el intervalo contiene números positivos
	 */
	@Test
	public void calculateMidPoint_PositiveNumbersTest() {
		assertEquals(5, IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).calculateMidPoint(), 0.0);
		assertEquals(5, IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(5, IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(5, IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).calculateMidPoint(), 0.0);
	}

	/*
	 * El siguiente test prueba el "cálculo del punto medio" para los 4 tipos de
	 * intervalos cuando el intervalo contiene números negativos
	 */
	@Test
	public void calculateMidPoint_NegativeNumbersTest() {
		assertEquals(-10, IntervalFactory.getInterval(-15, -5, IntervalType.BOTH_OPENED).calculateMidPoint(), 0.0);
		assertEquals(-10, IntervalFactory.getInterval(-15, -5, IntervalType.LEFT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(-10, IntervalFactory.getInterval(-15, -5, IntervalType.RIGHT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(-10, IntervalFactory.getInterval(-15, -5, IntervalType.UNOPENED).calculateMidPoint(), 0.0);
	}

	/*
	 * El siguiente test prueba el "cálculo del punto medio" para los 4 tipos de
	 * intervalos cuando el intervalo contiene números positivos y negativos
	 */
	@Test
	public void calculateMidPoint_TestPositiveNegativeNumbers() {
		assertEquals(0, IntervalFactory.getInterval(-10, 10, IntervalType.BOTH_OPENED).calculateMidPoint(), 0.0);
		assertEquals(0, IntervalFactory.getInterval(-10, 10, IntervalType.LEFT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(0, IntervalFactory.getInterval(-10, 10, IntervalType.RIGHT_OPENED).calculateMidPoint(), 0.0);
		assertEquals(0, IntervalFactory.getInterval(-10, 10, IntervalType.UNOPENED).calculateMidPoint(), 0.0);
	}

	/*
	 * El siguiente test prueba la evaluación de si un número está contenido en un
	 * intervalo BOTH_OPENED
	 */
	@Test
	public void includesValue_BothOpenedIntervalTest() {
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).includesValue(-3));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).includesValue(0));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).includesValue(5));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).includesValue(10));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.BOTH_OPENED).includesValue(13));
	}

	/*
	 * El siguiente test prueba la evaluación de si un número está contenido en un
	 * intervalo LEFT_OPENED
	 */
	@Test
	public void includesValue_LeftOpenedIntervalTest() {
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).includesValue(-3));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).includesValue(0));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).includesValue(5));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).includesValue(10));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.LEFT_OPENED).includesValue(13));
	}

	/*
	 * El siguiente test prueba la evaluación de si un número está contenido en un
	 * intervalo RIGHT_OPENED
	 */
	@Test
	public void includesValue_RightOpenedIntervalTest() {
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).includesValue(-3));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).includesValue(0));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).includesValue(5));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).includesValue(10));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.RIGHT_OPENED).includesValue(13));
	}

	/*
	 * El siguiente test prueba la evaluación de si un número está contenido en un
	 * intervalo UNOPENED
	 */
	@Test
	public void includesValue_UnOpenedIntervalTest() {
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).includesValue(-3));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).includesValue(0));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).includesValue(5));
		assertTrue(IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).includesValue(10));
		assertFalse(IntervalFactory.getInterval(0, 10, IntervalType.UNOPENED).includesValue(13));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * contiene otro intervalo con apertura BOTH_OPENED
	 */

	@Test
	public void includesInterval_BothOpenedInAnotherBothOpenTest() {
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * contiene otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void includesInterval_LeftOpenedInAnotherBothOpenTest() {
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * contiene otro intervalo con apertura RIGH_OPENED
	 */
	@Test
	public void includesInterval_RightOpenedInAnotherBothOpenTest() {
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * contiene otro intervalo con apertura UNOPENED
	 */
	@Test
	public void includesInterval_UnopenedTestInAnotherBothOpenTest() {
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * contiene otro intervalo con apertura BOTH_OPENED
	 */

	@Test
	public void includesInterval_BothOpenedInAnotherLeftOpenTest() {
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * contiene otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void includesInterval_LeftOpenedInAnotherLeftOpenTest() {
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * contiene otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void includesInterval_RightOpenedInAnotherLeftOpenTest() {
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * contiene otro intervalo con apertura UNOPENED
	 */
	@Test
	public void includesInterval_UnopenedInAnotherLeftOpenTest() {
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED contiene otro intervalo con apertura BOTH_OPENED
	 */

	@Test
	public void includesInterval_BothOpenedInAnotherRightOpenTest() {
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED contiene otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void includesInterval_LeftOpenedInAnotherRightOpenTest() {
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED contiene otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void includesInterval_RightOpenedInAnotherRightOpenTest() {
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED contiene otro intervalo con apertura UNOPENED
	 */
	@Test
	public void includesInterval_UnopenedInAnotherRightOpenTest() {
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * contiene otro intervalo con apertura BOTH_OPENED
	 */

	@Test
	public void includesInterval_BothOpenedInAnotherUnopenedTest() {
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * contiene otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void includesInterval_LeftOpenedInAnotherUnopenedTest() {
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.LEFT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.LEFT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.LEFT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * contiene otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void includesInterval_RightOpenedInAnotherUnopenedTest() {
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.RIGHT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.RIGHT_OPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * contiene otro intervalo con apertura UNOPENED
	 */
	@Test
	public void includesInterval_UnopenedInAnotherUnopenedTest() {

		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(10, 15, IntervalType.UNOPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(15, 20, IntervalType.UNOPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(20, 25, IntervalType.UNOPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(25, 30, IntervalType.UNOPENED)));
		assertTrue(unopenedPivot.includesInterval(IntervalFactory.getInterval(30, 35, IntervalType.UNOPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(35, 40, IntervalType.UNOPENED)));
		assertFalse(unopenedPivot.includesInterval(IntervalFactory.getInterval(40, 45, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * interseta a otro intervalo con apertura BOTH_OPENED
	 */
	@Test
	public void intersectsWith_BothOpenedIntervalWithAnotherBothOpenedTest() {

		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.BOTH_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.BOTH_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * interseta a otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void intersectsWith_BothOpenedIntervalWithAnotherLeftOpenedTest() {

		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.LEFT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.LEFT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * intersecta a otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void intersectsWith_BothOpenedIntervalWithAnotherRightOpenedTest() {

		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.RIGHT_OPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.RIGHT_OPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo BOTH_OPENED
	 * intersecta a otro intervalo con apertura UNOPENED
	 */
	@Test
	public void intersectsWith_BothOpenedIntervalInAnotherUnopenedTest() {

		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.UNOPENED)));
		assertTrue(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.UNOPENED)));
		assertFalse(bothOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * intersecta a otro intervalo con apertura BOTH_OPENED
	 */
	@Test
	public void intersectsWith_LeftOpenedIntervalWithAnotherBothOpenedTest() {

		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.BOTH_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.BOTH_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * intersecta a otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void intersectsWith_LeftOpenedIntervalWithAnotherLeftOpenedTest() {

		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.LEFT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.LEFT_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * intersecta a otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void intersectsWith_LeftOpenedIntervalWithAnotherRightOpenedTest() {

		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.RIGHT_OPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.RIGHT_OPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo LEFT_OPENED
	 * intersecta a otro intervalo con apertura UNOPENED
	 */
	@Test
	public void intersectsWith_LeftOpenedIntervalWithAnotherUnopenedTest() {

		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.UNOPENED)));
		assertTrue(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.UNOPENED)));
		assertFalse(leftOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED intersecta a otro intervalo con apertura BOTH_OPENED
	 */
	@Test
	public void intersectsWith_RightOpenedIntervalWithAnotherBothOpenedTest() {

		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.BOTH_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.BOTH_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED intersecta a otro intervalo con apertura LEFT_OPENED
	 */
	@Test
	public void intersectsWith_RightOpenedIntervalWithAnotherLeftOpenedTest() {

		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.LEFT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.LEFT_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED intersecta a otro intervalo con apertura RIGHT_OPENED
	 */
	@Test
	public void intersectsWith_RightOpenedIntervalWithAnotherRightOpenedTest() {

		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.RIGHT_OPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.RIGHT_OPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo
	 * RIGHT_OPENED intersecta a otro intervalo con apertura UNOPENED
	 */
	@Test
	public void intersectsWith_RightOpenedIntervalWithAnotherUnopenedTest() {

		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.UNOPENED)));
		assertTrue(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.UNOPENED)));
		assertFalse(rightOpenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.UNOPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * intersecta a otro intervalo con apertura BOTH_OPENED
	 */

	@Test
	public void intersectsWith_UnopenedIntervalWithAnotherBothOpenedTest() {

		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.BOTH_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.BOTH_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.BOTH_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * intersecta a otro intervalo con apertura LEFT_OPENED
	 */

	@Test
	public void intersectsWith_UnopenedIntervalWithAnotherLeftOpenedTest() {

		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.LEFT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.LEFT_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.LEFT_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.LEFT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * intersecta a otro intervalo con apertura RIGHT_OPENED
	 */

	@Test
	public void intersectsWith_UnopenedIntervalWithAnotherRightOpenedTest() {

		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.RIGHT_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.RIGHT_OPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.RIGHT_OPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.RIGHT_OPENED)));
	}

	/*
	 * El siguiente test prueba la evaluación de si un intervalo de tipo UNOPENED
	 * intersecta a otro intervalo con apertura UNOPENED
	 */

	@Test
	public void intersectsWith_UnopenedIntervalWithAnotherUnopenedTest() {

		assertFalse(
				unopenedPivotForIntersection.intersectsWith(IntervalFactory.getInterval(5, 15, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(10, 20, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(15, 25, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(20, 30, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(25, 35, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(30, 40, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(35, 45, IntervalType.UNOPENED)));
		assertTrue(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(40, 50, IntervalType.UNOPENED)));
		assertFalse(unopenedPivotForIntersection
				.intersectsWith(IntervalFactory.getInterval(45, 55, IntervalType.UNOPENED)));
	}

}